import com.google.gson.JsonElement
import com.google.gson.JsonObject
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.post
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.runBlocking

operator fun JsonElement.get(key:String): JsonElement? = this.asJsonObject[key]
operator fun JsonElement.get(key:Int): JsonElement? = this.asJsonArray[key]


@KtorExperimentalAPI
fun main() {
    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
    }
    val wtf = runBlocking {
        client.post<JsonObject>("http://overpass-api.de/api/interpreter") {
            body = TextContent("""
[out:json];
node
  [public_transport=station]
  (48.99328437970295, 8.463899459838867, 49.02908949178494, 8.474578857421875);
out;
""", contentType = ContentType.Text.Any)
        }
    }
    println(wtf["elements"].asJsonArray .map { it["lat"] to it["lon"] to it["tags"]!!["name"]})
    println(wtf["elements"][0]!!["lat"])
}
