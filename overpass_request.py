import urllib.request as rq
import urllib.parse as ps

data = b"data=" + ps.quote_plus((
"""
[out:json];
node
  [public_transport=station]
  (48.99328437970295, 8.463899459838867, 49.02908949178494, 8.474578857421875);
out;
""")).encode("utf-8")
print(data)

#exit(1)
resp = rq.urlopen("http://overpass-api.de/api/interpreter", data=data)
print(resp.read(2000).decode("utf-8"))
